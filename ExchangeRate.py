# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 12:52:35 2018

@author: user009

DataVer  - Author - Note
20180912 - C.Y. Peng - First Release
20181003 - C.Y. Peng - add the H5 file format
20190310 - C.Y. Peng - add the txt file format
"""

#import requests
from sys import path
path.append('../CommonLib/')
import H5FileManager as H5FM
import DataCircularMatrix as DCM
import numpy as np
import pandas
import os

def main(setFileName, setOutputFolder):
    CER = GetExchangeRate(setFileName, setOutputFolder)
    CER.getTaiwanBankExchangeRate()
    CER.getTaiwanBankExchangeRate2TxT()

class GetExchangeRate():
    def __init__(self, FileName = 'FinanceData', OutputFolder = '.\\Analysis\\'):
        self.html = "http://rate.bot.com.tw/xrt?Lang=zh-TW"
        #dfs = pandas.read_html("https://ebank.landbank.com.tw/infor/infor.aspx")
        #res = requests.get("https://http://rate.bot.com.tw/xrt?Lang=zh-TW")
        #print(res.text)
        self.OriDataFrame = None
        self.ExchangeRateDataFrame = None
        self.FileName = FileName
        self.Dict = {}
        self.Data = None
        self.output_folder = OutputFolder
    
    def __getHtmlData__(self):
        self.OriDataFrame = pandas.read_html(self.html)
    
    def getTaiwanBankExchangeRate(self):
        self.__getHtmlData__()
        self.ExchangeRateDataFrame = self.OriDataFrame[0]
        self.ExchangeRateDataFrame = self.ExchangeRateDataFrame.iloc[:,0:5]
        self.ExchangeRateDataFrame.columns = [u"Currency",u"Current-Buying Rate",u"Current-Selling Rate",u"Spot-Buying Rate",u"Spot-Selling Rate"]
        self.ExchangeRateDataFrame[u"Currency"] = self.ExchangeRateDataFrame[u"Currency"].str.extract("([A-Z]+)")
        
        self.Dict['ExchangeRateTable'] = DCM.StringArrayCircularMatrix()
        DM = self.Dict['ExchangeRateTable']
        thisData = " ".join(str(x) for x in self.ExchangeRateDataFrame.columns)
        DM.putString(thisData)
        ExchangeRateDataFrame = np.array(self.ExchangeRateDataFrame)
        for idx in range(0, len(ExchangeRateDataFrame)):
            thisData = " ".join(str(x) for x in ExchangeRateDataFrame[idx])
            DM.putString(thisData)

        self.Data = DM.getStringArray()
        H5FM.StringArray2H5(self.Data, self.FileName, 'ExchangeRateTable', groupName = 'ExchangeRate')

        #print(self.ExchangeRateDataFrame)
        print('Get Exchange Rate and Save Files! (.h5)')

    def getTaiwanBankExchangeRate2TxT(self):
        pandas.DataFrame(self.Data).to_csv(os.path.join(self.output_folder, "ExchangeRate.csv"), header=True, index=False, sep=',', #\t
                                           mode='w')
        #file = open(self.output_folder + "ExchangeRate.txt", "w")
        #file.write(pandas.DataFrame(self.Data))
        print('Get Exchange Rate and Save Files (.txt)!')
