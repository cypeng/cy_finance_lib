# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 12:38:58 2018

@author: user009
DataVer  - Author - Note
20181002 - C.Y. Peng - First Release
20190213 - C.Y. Peng - add the warning
20190415 - C.Y. Peng - Auto finding the CSS tage name
20200107 - C.Y. Peng - add Web Info File Argument
"""
from sys import path
#path.append('../CommonLib/')
import CommonUtility as CU
import cnyesFund as cFund
import codecs
import os

def main(setFileName, OutputFolder, webInfoPath, webInfoFile = 'WebAccessInf'):
    webInfoFile = webInfoFile + '.txt'
    FundWebInfList, RawFundWebInfList = readFile(os.path.join(webInfoPath, webInfoFile))
    writeFile(os.path.join(webInfoPath, 'New'+webInfoFile), RawFundWebInfList[0])
    #print(RawFundWebInfList)
    #print(FundWebInfList)
    for idx in range(1, len(FundWebInfList)):
        a, b, c = getFundInf(FundWebInfList[idx][0], FundWebInfList[idx][1], FundWebInfList[idx][2], FundWebInfList[idx][3], FundWebInfList[idx][4], OutputFolder, setFileName, idx)
        thisWriteStr = RawFundWebInfList[idx]
        thisWriteStrArray = thisWriteStr.split(",")
        thisWriteStrArray[2], thisWriteStrArray[3], thisWriteStrArray[4] = a, b, c
        writeFile(os.path.join(webInfoPath, 'New'+webInfoFile), ','.join(thisWriteStrArray)+'\n')
    os.remove(os.path.join(webInfoPath, webInfoFile))
    os.rename(os.path.join(webInfoPath, 'New'+webInfoFile), os.path.join(webInfoPath, webInfoFile))

def getFundInf(FundName, URL, PerformanceTableTagName, HistoryNAVTagName, HistoryNAVPageTagName, OutputFolder, setFileName, thisCount):
    FundInf = cFund.main(FundName, URL, PerformanceTableTagName, HistoryNAVTagName, HistoryNAVPageTagName, OutputFolder,
                         setFileName, thisCount)
    if (FundInf.PerformanceTableCSStag == None) or (FundInf.HistoryNAVCSStag == None) or (FundInf.HistoryNAVPageCSStag == None):
        print("[Warning] " + FundName + " : Web Access Information Need Update!")

    #print(FundInf.PerformanceTableCSStag, FundInf.HistoryNAVCSStag, FundInf.HistoryNAVPageCSStag)
    return FundInf.PerformanceTableCSStag, FundInf.HistoryNAVCSStag, FundInf.HistoryNAVPageCSStag
    """
    try:
        createOutputFolder(OutputFolder+FundName+"\\")
        FundInf = cFund.main(FundName, URL, PerformanceTableTagName, HistoryNAVTagName, HistoryNAVPageTagName, OutputFolder, setFileName, thisCount)
    except:
        print("[Warning] " + FundName + " : Web Access Information Need Update!")
        pass
    """

def writeFile(WebFileName, str):
    NewWebInfFile = codecs.open(WebFileName, 'a')
    NewWebInfFile.write(str)
    NewWebInfFile.close()

def readFile(WebFileName):
    WebInfFile = codecs.open(WebFileName)
    Done  = bool(0)
    FundWebInfList = []
    RawFundWebInfList = []
    while (not Done):
        thisline = WebInfFile.readline()
        RawFundWebInfList.append(thisline)
        LineInf = thisline.strip("\n")
        LineInfArray = LineInf.split(',')
        
        if (len(LineInfArray) == 1):
            Done = bool(1)
            WebInfFile.close()
        else:   
            FundWebInfList.append(LineInfArray)
            
    return FundWebInfList, RawFundWebInfList
            
"""
if __name__== "__main__":
    main()
"""