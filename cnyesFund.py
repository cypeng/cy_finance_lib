# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 11:35:23 2018

Ref.
https://www.dataquest.io/blog/tutorial-time-series-analysis-pandas/

@author: user009
DataVer  - Author - Note
20181002 - C.Y. Peng - First Release
20181024 - C.Y. Peng - Performance Table -> Table
20190117 - C.Y. Peng - Fund Name Ver  - Self Set Fund Name for Input Argument
20190221 - C.Y. Peng - Setting: Output Folder and ML Prediction
20190310 - C.Y. Peng - add the txt file format
20190415 - C.Y. Peng - Auto finding the CSS tage name
20200107 - C.Y. Peng - add the csv file format
"""
from sys import path
path.append('../CommonLib/')
import CommonUtility as CU
from selenium import webdriver
from bs4 import BeautifulSoup
import numpy as np
import time
import pandas
import DataCircularMatrix as DCM
import H5FileManager as H5FM
from datetime import datetime
import h5py
import seaborn as sns
import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cbook as cbook
import seaborn as sns
import chardet
import re
import os

def main(FundName, URL, PerformanceTableCSStag, HistoryNAVCSStag, HistoryNAVPageCSStag, OutputFolder, setFileName, thisCount):
    CU.createOutputFolder(os.path.join(OutputFolder, FundName))
    cFund = cnyesFund(FundName, URL, thisCount, OutputFolder, FileName = setFileName)
    cFund.JavaScriptParserIniSetting()
    cFund.getWebSource()
    cFund.getFundName()
    cFund.getPerformanceTable(PerformanceTableCSStag)
    cFund.getNewHistoryNAV(HistoryNAVCSStag, HistoryNAVPageCSStag)
    cFund.plotNAV()
    cFund.getData2TxT()
    return cFund

class cnyesFund():
    def __init__(self, FundName, URL, Count, OutputFolder = '.\\Analysis\\', FileName = 'FinanceData'):
        self.url = URL
        self.driver = None
        self.pageSource = None
        self.WebContent = None
        self.FundName = FundName
        self.PerformanceTable = None
        self.HistoryNAV = None
        self.lastUpdateDate = None
        self.FileName = FileName
        self.Dict = {}
        self.groupName = r'Fund/'
        self.thisCount = Count
        self.performance_matrix = None
        self.nav_data = None
        self.output_folder = OutputFolder
        self.PerformanceTableCSStag = None
        self.HistoryNAVCSStag = None
        self.HistoryNAVPageCSStag = None
        
    def JavaScriptParserIniSetting(self):
        pathChromedriver = r'chromedriver.exe'
        chrome_options=webdriver.ChromeOptions()
        chrome_options.set_headless()
        chrome_options.add_argument('--disable-gpu')
        self.driver=webdriver.Chrome(pathChromedriver,options=chrome_options)
        
    def getWebSource(self):
        self.driver.get(self.url)
        self.__updateWebPage__()
    
    def getFundName(self):
        #self.FundName = unicode(self.WebContent.find('h1').get_text())
        #self.FundName = self.FundName.encode('UTF-8')
        #self.FundName = self.WebContent.find('h1').get_text()
        #print(self.FundName, type(self.FundName))
        self.groupName = r'Fund/' + self.FundName
        #print(self.FundName, type(self.FundName), type(self.groupName))
        self.__recordFundList__()
        
    def __recordFundList__(self):
        FileName = self.FileName
        self.Dict['FundList'] = DCM.StringArrayCircularMatrix()
        DM = self.Dict['FundList']
        thisData = self.FundName
        DM.putString(thisData)
        thisFundName = DM.getStringArray()
        if (1 == self.thisCount): 
            H5FM.StringArray2H5(thisFundName, FileName, 'FundList', groupName = 'Fund')
        else:
            StringTemp = np.concatenate((H5FM.H52StringArray(FileName, 'FundList', groupName = 'Fund'), thisFundName),axis = 0)
            H5FM.StringArray2H5(StringTemp, FileName, 'FundList', groupName = 'Fund')  
        
    def getPerformanceTable(self, TagName):
        dataType = np.dtype([('Title', h5py.special_dtype(vlen=str)), ('One Month', h5py.special_dtype(vlen=str)), 
                             ('Three Months', h5py.special_dtype(vlen=str)), ('Six Months', h5py.special_dtype(vlen=str)),
                             ('This Year', h5py.special_dtype(vlen=str)), ('One Year', h5py.special_dtype(vlen=str)),
                             ('Three Years', h5py.special_dtype(vlen=str)), ('Five Years', h5py.special_dtype(vlen=str)),
                             ('Ten Years', h5py.special_dtype(vlen=str))])
        
        FileName = self.FileName
        self.PerformanceTable,  self.PerformanceTableCSStag = self.__getHTMLTableWithCheck__(TagName)
        #print(self.PerformanceTableCSStag)
        #print(self.PerformanceTable)
        print(self.PerformanceTable)
        PerformanceMatrix = np.zeros((len(self.PerformanceTable[0].values),), dtype=dataType)
        #self.Dict[self.FundName + 'PerformanceTable'] = DCM.StringArrayCircularMatrix()
        #DM = self.Dict[self.FundName + 'PerformanceTable']
        thisData = " ".join(str(x) for x in self.PerformanceTable[0])
        #DM.putString(thisData)
        for idx in range(0, len(self.PerformanceTable[0].values)):
            thisData = " ".join(str(x) for x in self.PerformanceTable[0].values[idx])
            thisArray = thisData.split()
            PerformanceMatrix['Title'][idx] = thisArray[0]
            #PerformanceMatrix['Title'][idx] = thisArray[0].encode('utf-8').decode('unicode_escape')
            #PerformanceMatrix['One Month'][idx] = thisArray[1]
            PerformanceMatrix['Three Months'][idx] = thisArray[2]
            PerformanceMatrix['Six Months'][idx] = thisArray[3]
            PerformanceMatrix['This Year'][idx] = thisArray[4]
            PerformanceMatrix['One Year'][idx] = thisArray[5]
            PerformanceMatrix['Three Years'][idx] = thisArray[6]
            PerformanceMatrix['Five Years'][idx] = thisArray[7]
            PerformanceMatrix['Ten Years'][idx] = thisArray[8]
            #DM.putString(thisData)
        #print(FileName, self.groupName)
        H5FM.Data2H5(PerformanceMatrix, FileName, 'Performance Table', groupName = self.groupName, dtype = dataType)
        self.performance_matrix = PerformanceMatrix
        #H5FM.StringArray2H5(DM.getStringArray(), FileName, 'PerformanceTable', groupName = self.groupName)
        #print(DM.getStringArray())
    
    def getNewHistoryNAV(self, CSSTableTagName, CSSPageTagName):
        FileName = self.FileName
        self.__checkUpdHistoryNAV__(FileName)
        temp, self.HistoryNAVCSStag = self.__getHTMLTableWithCheck__(CSSTableTagName)
        #print(temp, self.HistoryNAVCSStag)
        temp, self.HistoryNAVPageCSStag = self.__checkToNextPage__(CSSPageTagName)
        #print(temp, CSSPageTagName, self.HistoryNAVPageCSStag)
        if (None == self.lastUpdateDate):
            print('Load History NAV', self.FundName)
            self.__loadHistoryNAV__(FileName, self.HistoryNAVCSStag, self.HistoryNAVPageCSStag)
        else:
            print('Update History NAV', self.FundName)
            self.__updateHistoryNAV__(FileName, self.HistoryNAVCSStag, self.HistoryNAVPageCSStag)
        print('Completed!')
        self.__getHistoryNAVMatrix__()

    def getData2TxT(self):
        pandas.DataFrame(self.performance_matrix).to_csv(os.path.join(self.output_folder, self.FundName, self.FundName+"_Performance.csv"),
                                                         header=True,
                                                         index=False, 
                                                         sep=',',
                                                         mode='w')
        NAVMatrix = H5FM.H52Data(self.FileName, 'NAV_Matrix', groupName=self.groupName)
        pandas.DataFrame(NAVMatrix).to_csv(os.path.join(self.output_folder,self.FundName, self.FundName + "_HistoryNAV.csv"),
                                           header=True,
                                           index=False,
                                           sep=',',
                                           mode='w')
        #file = open(self.output_folder + self.FundName + "_Performance.txt", "w")
        #file.write(str(self.performance_matrix))
        print('Save Files (.csv)!')

    def __getHistoryNAVMatrix__(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), ('Up/Down', np.float),
                             ('Up/Down-Percentage', h5py.special_dtype(vlen=str)),('Up/Down Day', np.float)])
        strData = H5FM.H52StringArray(self.FileName, 'HistoryNAV', groupName = self.groupName)
        NAVData = np.zeros((len(strData),), dtype=dataType)
        day = 0
        for idx in range(0, len(strData)):
            kdx = len(strData) -1 - idx
            thisStr = strData[kdx]
            thisStrArray = thisStr.split()
            """
            temp = time.strptime(thisStrArray[0],'%Y/%m/%d')
            if (temp[1] < 10):
                strtemp1 = '0' + str(temp[1])
            else:
                strtemp1 = str(temp[1])
            if (temp[2] < 10):
                strtemp2 = '0' + str(temp[2])
            else:
                strtemp2 = str(temp[2])
            temp = str(temp[0]) + strtemp1 + strtemp2
            """
            NAVData['Date'][kdx] = thisStrArray[0]
            NAVData['NAV'][kdx] = float(thisStrArray[1])
            NAVData['Up/Down'][kdx] = float(thisStrArray[2])
            NAVData['Up/Down-Percentage'][kdx] = thisStrArray[3] 
            day += 1
            if (kdx <= len(strData) - 2):
                if (NAVData['Up/Down'][kdx] >= 0) and (NAVData['Up/Down'][kdx+1] <= 0):
                    day = 0
                elif (NAVData['Up/Down'][kdx] <= 0) and (NAVData['Up/Down'][kdx+1] >= 0):
                    day = 0
                    
            NAVData['Up/Down Day'][kdx] = day
        H5FM.Data2H5(NAVData, self.FileName, 'NAV_Matrix', groupName = self.groupName, dtype = dataType)
        self.nav_data = NAVData


    def plotNAV(self):
        #years = mdates.YearLocator()  # every year
        #months = mdates.MonthLocator()  # every month
        #yearsFmt = mdates.DateFormatter('%Y')
        sns.set(rc={'figure.figsize': (11, 4)})
        pd_data = pd.DataFrame(self.nav_data[::-1])
        pd_data['Date'] = pd.to_datetime(pd_data['Date'])
        #print(pd.to_datetime(pd_data['Date']))
        pd_data.plot(x = 'Date', y= 'NAV')
        plt.xlabel('Date, year')
        plt.ylabel('Fund Net Asset Value')
        plt.title(self.FundName + ' NAV Plot')
        plt.show(block=False)
        plt.savefig(os.path.join(self.output_folder, self.FundName, self.FundName+'_NAV.svg'))
        plt.close()

        #pd_data['NAV '].set_index('Date').plot()
        #plt.show()
        #plt.close()
        #nav_timeseries = pd.Series(pd_data['NAV '], index = pd_data['Date'])
        #plt.style.use('ggplot')
        #nav_timeseries['NAV '].plot()
        #pd.to_datetime(data['Date'], format = '%Y/%m/%d')
        #pd.Series.plot(x = 'Date', y = 'NAV ')
        #data.plot(x = 'Date', y = 'NAV ')
        #print(data['Date'])
        #plt.show(block=False)
        #plt.close()


    
    def __updateHistoryNAV__(self, FileName, CSSTableTagName, CSSPageTagName):
        self.HistoryNAV = self.__getHTMLTable__(CSSTableTagName)
        Data = np.array(self.HistoryNAV[0])
        DoneFlag = bool(0)
        while (not DoneFlag):
            if (not self.FundName in self.Dict):
                self.Dict[self.FundName] = DCM.StringArrayCircularMatrix()
                DM = self.Dict[self.FundName]     
                for idx in range(0, len(Data)):
                    #print(" ".join(str(x) for x in Data[idx]))
                    thisData = " ".join(str(x) for x in Data[idx])
                    thisDataArray = thisData.split()
                    #print(thisDataArray[0])
                    if (self.lastUpdateDate >= datetime.strptime(thisDataArray[0],'%Y/%m/%d')):
                        DoneFlag = bool(1)
                        break
                    DM.putString(thisData)     
            else:
                DM = self.Dict[self.FundName]
                for idx in range(0, len(Data)):
                    #print(" ".join(str(x) for x in Data[idx]))
                    thisData = " ".join(str(x) for x in Data[idx])
                    thisDataArray = thisData.split()
                    if (self.lastUpdateDate >= datetime.strptime(thisDataArray[0],'%Y/%m/%d')):
                        DoneFlag = bool(1)
                        break
                    DM.putString(thisData)
                    if (1 == DM.isCountLimit()):
                        StringTemp = np.concatenate((DM.getStringArray(), H5FM.H52StringArray(FileName, 'HistoryNAV', groupName = self.groupName)),axis = 0)
                        H5FM.StringArray2H5(StringTemp, FileName, 'HistoryNAV', groupName = self.groupName)
            DoneFlag = ((DoneFlag or (not self.__toNextPage__(CSSPageTagName))))
            if (not DoneFlag):
                self.HistoryNAV = self.__getHTMLTable__(CSSTableTagName)
                Data = np.array(self.HistoryNAV[0])
            
        if (not DM.isCountLimit()):
            StringTemp = np.concatenate((DM.getStringArray(), H5FM.H52StringArray(FileName, 'HistoryNAV', groupName = self.groupName)),axis = 0)
            H5FM.StringArray2H5(StringTemp, FileName, 'HistoryNAV', groupName = self.groupName)
    
    def __loadHistoryNAV__(self, FileName, CSSTableTagName, CSSPageTagName):
        self.HistoryNAV = self.__getHTMLTable__(CSSTableTagName)
        Data = np.array(self.HistoryNAV[0])
        DoneFlag = bool(0)
        #updateDoneFlag = bool(0)
        while (not DoneFlag):
            if (not self.FundName in self.Dict):
                self.Dict[self.FundName] = DCM.StringArrayCircularMatrix()
                DM = self.Dict[self.FundName]     
                for idx in range(0, len(Data)):
                    #print(" ".join(str(x) for x in Data[idx]))
                    thisData = " ".join(str(x) for x in Data[idx])
                    DM.putString(thisData)
                    if (idx == 0):
                        H5FM.StringArray2H5(DM.getStringArray(), FileName, 'HistoryNAV', groupName = self.groupName) 
            else:
                DM = self.Dict[self.FundName]
                for idx in range(0, len(Data)):
                    #print(" ".join(str(x) for x in Data[idx]))
                    thisData = " ".join(str(x) for x in Data[idx])
                    DM.putString(thisData)
                    if (1 == DM.isCountLimit()):
                        StringTemp = np.concatenate((H5FM.H52StringArray(FileName, 'HistoryNAV', groupName = self.groupName), DM.getStringArray()),axis = 0)
                        H5FM.StringArray2H5(StringTemp, FileName, 'HistoryNAV', groupName = self.groupName)
            DoneFlag = (not self.__toNextPage__(CSSPageTagName))
            if (not DoneFlag):
                self.HistoryNAV = self.__getHTMLTable__(CSSTableTagName)
                Data = np.array(self.HistoryNAV[0])

        #if ((not updateDoneFlag) and (not DM.isCountLimit())):
        if (not DM.isCountLimit()):
            StringTemp = np.concatenate((H5FM.H52StringArray(FileName, 'HistoryNAV', groupName = self.groupName), DM.getStringArray()),axis = 0)
            H5FM.StringArray2H5(StringTemp, FileName, 'HistoryNAV', groupName = self.groupName)

    # 2019/04/15 Check the HTML Table - Auto finding the CSS tage name
    def __getHTMLTableWithCheck__(self, CSSTagName):
        num_array = CSSTagName.split(r'"')
        total_num = 100
        num = int(num_array[1])-int(total_num*0.5)
        for idx in range(1, total_num+1):
            table_source = self.__getHTMLTable__(CSSTagName)
            if (table_source == None):
                num += 1
                num_array[1] = str(num)
                #print(num_array)
                CSSTagName = '"'.join(num_array)
            else:
                return table_source, CSSTagName
        return None, None

    def __checkUpdHistoryNAV__(self, FileName):
        try:
            Data = H5FM.H52StringArray(FileName, 'HistoryNAV', groupName = self.groupName)
            DataArray = Data[0][:].split()
            self.lastUpdateDate = datetime.strptime(DataArray[0],'%Y/%m/%d')
        except:
            pass

    # 2019/04/15 Check To Next Page - Auto finding the CSS tage name
    def __checkToNextPage__(self, CSSTagName):
        num_array = CSSTagName.split(r'"')
        total_num = 100
        num = int(num_array[1])-int(total_num*0.5)
        flag = bool(0)
        for idx in range(1, total_num+1):
            try:
                element = self.driver.find_element_by_css_selector(CSSTagName)
                #print(element.text, CSSTagName)
                if (element.text == "下一頁"):
                    flag = bool(1)
                    return flag, CSSTagName
            except:
                pass
            num += 1
            num_array[1] = str(num)
            CSSTagName = '"'.join(num_array)

        return flag, "None"

    def __toNextPage__(self, CSSPageTagName):
        try:
            self.driver.find_element_by_css_selector(CSSPageTagName).click()
            self.__updateWebPage__()
            return bool(1)
        except:
            return bool(0)
    
    def __getHTMLTable__(self, CSSTableTagName):
        tableSource = self.WebContent.select(CSSTableTagName)
        #print(self.WebContent)
        try:
            return pandas.read_html(str(tableSource))
        except:
            return None
     
    def __updateWebPage__(self):
        #self.driver.implicitly_wait(30)
        time.sleep(5)
        self.pageSource = self.driver.page_source
        #self.driver.implicitly_wait(5)
        #webdriver.support.ui.WebDriverWait(self.driver, timeout = 30).until(webdriver.support.expected_conditions.staleness_of(self.pageSource))
        self.WebContent = BeautifulSoup(self.pageSource, 'html.parser')
