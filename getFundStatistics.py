# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 18:43:00 2018

@author: user009
Reference:
    1. https://www.ezchart.com.tw/inds.php?IND=RSI, Statistic of Fund
    
DataVer  - Author - Note
20181003 - C.Y. Peng - First Release
20181005 - C.Y. Peng - Quartile
20181018 - C.Y. Peng - History Quartile/DIF/MACD/OSC/MA
20181022 - C.Y. Peng - History KD/ RSI
20181025 - C.Y. Peng - History MTM/ W%R/ Bias
20181031 - C.Y. Peng - History OBV/ SAR
20190310 - C.Y. Peng - add the txt file format
20200107 - C.Y. Peng - add the csv file format
20200109 - C.Y. Peng - add the train/test file format
"""
from sys import path
#path.append('../CommonLib/')
import H5FileManager as H5FM
import CommonUtility as CU
import numpy as np
#import time
import h5py
import pandas
import os

def main(fileName, datesetName, groupName, setOutputFolder):
    FundList = H5FM.H52StringArray(fileName, datesetName, groupName = groupName)
    for idx in range(0, len(FundList)):
        fundName = FundList[idx]
        #CU.createOutputFolder(os.path.join(setOutputFolder, fundName))
        print('Get Some Fundamental Analysis for Fund: ' + fundName)
        FS = getFundStatiscs(fileName, fundName, groupName, setOutputFolder)
        FS.readData()
        FS.getQuartile()
        FS.getMovingAverage()
        FS.getHistoryRSI()
        FS.getHistoryOSC()
        FS.getHistoryKD()
        FS.getHistoryWR()
        FS.getHistoryMTM()
        FS.getHistoryBias()
        FS.getHistoryOBV()
        FS.getHistoryPSY()
        FS.getHistorySAR()
        FS.getData2TxT()
        FS.getAll()
        print('Completed!')

class getFundStatiscs():
    def __init__(self, fileName, fundName, folder, setOutputFolder = '.\\Analysis\\'):
        self.FileName = fileName
        self.DatesetName = 'HistoryNAV'
        self.FundName = fundName
        self.GroupName = folder +'/' + fundName
        self.NAVData = None
        self.NAVUpDown = None
        self.NAVQuartile = None
        self.NAVMA = None
        self.NAVKD = None
        self.NAVOSC = None
        self.NAVRSI = None
        self.NAVMTM = None
        self.NAVWR  = None
        self.NAVBias= None
        self.NAVOBV = None
        self.NAVPSY = None
        self.NAVSAR = None
        self.DateData = None
        self.OutputFolder = setOutputFolder

    def getData2TxT(self):
        pandas.DataFrame(self.NAVBias).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_BiasIndex.csv"),
                                              header=True,
                                              index=False, sep=',', #\t
                                              mode='w')
        pandas.DataFrame(self.NAVMTM).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_MomentomIndex.csv"),
                                              header=True,
                                              index=False, sep=',', #\t
                                              mode='w')
        pandas.DataFrame(self.NAVMA).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_MovingAverageIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVOBV).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_OnBalanceVolumeIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVOSC).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_OscillatorIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVPSY).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_PsychologicalLineIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVQuartile).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_QuartileIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVRSI).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_RelativeStrengthIndex.csv"),
                                                  header=True,
                                                  index=False, sep=',', #\t
                                                  mode='w')
        pandas.DataFrame(self.NAVSAR).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_StopAndReserveIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVWR).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_WilliamsOverboughtIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        pandas.DataFrame(self.NAVKD).to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_StochasticOscillatorIndex.csv"),
                                             header=True,
                                             index=False, sep=',', #\t
                                             mode='w')
        print('Save Files (.csv)!')

    def getAll(self):
        drop_list = ['Date', 'NAV']
        allData = pandas.concat([pandas.DataFrame(self.NAVBias), 
                                 pandas.DataFrame(self.NAVMTM).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVMA).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVOBV).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVOSC).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVPSY).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVQuartile).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVRSI).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVSAR).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVWR).drop(columns=drop_list),
                                 pandas.DataFrame(self.NAVKD).drop(columns=drop_list)], 
                                 axis=1, 
                                 join='inner')
        
        cols=pandas.Series(allData.columns)
        for dup in cols[cols.duplicated()].unique(): 
            cols[cols[cols == dup].index.values.tolist()] = [dup + '_' + str(i) if i >= 0 else dup for i in range(sum(cols == dup))]

        # rename the columns with the cols list.
        allData.columns=cols
        allData['Date'] = allData['Date'].str.replace('/','-')
        allData.to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_All.csv"),\
                       header=True,\
                       index=False, sep=',', #\t\
                       mode='w')
        self.__TimeseriesRawDataPrepraption__(allData)
        print('Save Files (.csv)!')

    def __TimeseriesRawDataPrepraption__(self, df_data):
        # Data Preparation
        #df_data = read_data(os.path.join(database_folder, filename))
        df_data['y'] = df_data.loc[:, 'NAV'].shift(periods=1)
        df_data = df_data.sort_values("Date",ascending= True)
        train_data = df_data.iloc[0:-2, :]
        test_data = df_data.tail(1).drop(columns=['y'])
        train_data.to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_All_train.csv"), index=False)
        test_data.to_csv(os.path.join(self.OutputFolder, self.FundName, self.FundName+"_All_test.csv"), index=False)

    def readData(self):
        NAVMatrix = H5FM.H52Data(self.FileName, 'NAV_Matrix', groupName = self.GroupName)
        (self.DateData, self.NAVData, self.NAVUpDown, _, _) = zip(*NAVMatrix)
        #print(DateData, NAVDate)
        #print(strData, NAVData)
    """
    Quartile Index
    """
    def getQuartile(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('min', np.float), ('Q1', np.float), 
                             ('Q2', np.float), ('Q3', np.float), ('Max', np.float),
                             ('Long/Short', h5py.special_dtype(vlen=str))])
    
        self.NAVQuartile = np.zeros((len(self.NAVData),), dtype = dataType)
        self.NAVQuartile['Date'] = self.DateData
        self.NAVQuartile['NAV'] = self.NAVData
        for idx in range(0, len(self.NAVData)):
            NAVQuartile = np.percentile(self.NAVData[idx:], [0, 25, 50, 75, 100])
            self.NAVQuartile['min'][idx] = NAVQuartile[0]
            self.NAVQuartile['Q1'][idx] = NAVQuartile[1]
            self.NAVQuartile['Q2'][idx] = NAVQuartile[2]
            self.NAVQuartile['Q3'][idx] = NAVQuartile[3]
            self.NAVQuartile['Max'][idx] = NAVQuartile[4]
            if (self.NAVData[idx] <= self.NAVQuartile['min'][idx]):
                self.NAVQuartile['Long/Short'][idx] = 'super-S+'
            if (self.NAVData[idx] > self.NAVQuartile['min'][idx]):
                self.NAVQuartile['Long/Short'][idx] = 'super-S'
            if (self.NAVData[idx] >= self.NAVQuartile['Q1'][idx]):
                self.NAVQuartile['Long/Short'][idx] = 'S'
            if (self.NAVData[idx] >= self.NAVQuartile['Q2'][idx]):
                self.NAVQuartile['Long/Short'][idx] = 'L'
            if (self.NAVData[idx] >= self.NAVQuartile['Q3'][idx]):
                self.NAVQuartile['Long/Short'][idx] = 'super-L'
            if (self.NAVData[idx] >= self.NAVQuartile['Max'][idx]):
                self.NAVQuartile['Long/Short'][idx] = 'super-L+'
        H5FM.Data2H5(self.NAVQuartile,  self.FileName, 'NAV_Quartile', groupName = self.GroupName, dtype = dataType)
    
    """
    MA Index
    """   
    def getMovingAverage(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('MA, Week', np.float), ('MA, Half of Month', np.float), 
                             ('MA, Month', np.float), ('MA, Season', np.float), ('MA, Year', np.float)])
    
        self.NAVMA = np.zeros((len(self.NAVData),), dtype = dataType)
        self.NAVMA['Date'] = self.DateData
        self.NAVMA['NAV'] = self.NAVData
        for idx in range(0, len(self.NAVData)):
            length_NAV_idx = len(self.NAVData)-1
            if (idx+4 <= length_NAV_idx):
                self.NAVMA['MA, Week'][idx] = np.mean(self.NAVData[idx:idx+5])

            if (idx+9 <= length_NAV_idx):
                self.NAVMA['MA, Half of Month'][idx] = np.mean(self.NAVData[idx:idx+10])
                
            if (idx+19 <= length_NAV_idx):
                self.NAVMA['MA, Month'][idx] = np.mean(self.NAVData[idx:idx+20])
                
            if (idx+59 <= length_NAV_idx):
                self.NAVMA['MA, Season'][idx] = np.mean(self.NAVData[idx:idx+60])
                
            if (idx+239 <= length_NAV_idx):
                self.NAVMA['MA, Year'][idx] = np.mean(self.NAVData[idx:idx+240])
                
        
        H5FM.Data2H5(self.NAVMA,  self.FileName, 'NAV_MovingAverage', groupName = self.GroupName, dtype = dataType)
    """
    SAR Index
    """
    def getHistorySAR(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('AF', np.float), ('UD', h5py.special_dtype(vlen=str)), 
                             ('SAR-10', np.float), ('K', np.float),
                             ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVSAR = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVSAR['Date'] = self.DateData
        self.NAVSAR['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVSAR['K'][kdx] = self.NAVKD['K'][kdx]
            self.NAVSAR['UD'][kdx] = self.__CalculateUD__(kdx)
            self.NAVSAR['SAR-10'][kdx] = self.__CalculateSAR__(kdx, 10)
            
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            K_Line = self.NAVSAR['K'][kdx]
            if (K_Line > self.NAVSAR['SAR-10'][kdx]):
                self.NAVSAR['Long/Short'][kdx] = "super-L" 
            if (K_Line < self.NAVSAR['SAR-10'][kdx]):
                self.NAVSAR['Long/Short'][kdx] = "super-S"
                
        H5FM.Data2H5(self.NAVSAR,  self.FileName, 'NAV_StopAndReserve', groupName = self.GroupName, dtype = dataType)

    def __CalculateUD__(self, thisIdx):
        UpDown = self.NAVUpDown[thisIdx]
        if (UpDown > 0):
            return 'U'
        elif (UpDown < 0):
            return 'D'
        else:
            if (thisIdx == len(self.NAVData)-1):
                return 0
            else:
                return self.NAVSAR['UD'][thisIdx+1]
        
    def __CalculateSAR__(self, thisIdx, day):
        if (thisIdx == len(self.NAVData)-1-day):
            self.NAVSAR['AF'][thisIdx] = 0.02
            if ('U' == self.NAVSAR['UD'][thisIdx]):
                return np.max(self.NAVSAR['NAV'][thisIdx:])
            elif ('D' == self.NAVSAR['UD'][thisIdx]):
                return np.min(self.NAVSAR['NAV'][thisIdx:])
            else:
                return 0
        elif (thisIdx < len(self.NAVData)-1-day):
            if ('U' == self.NAVSAR['UD'][thisIdx]) and ('D' == self.NAVSAR['UD'][thisIdx+1]):
                self.NAVSAR['AF'][thisIdx] = 0.02
            elif ('D' == self.NAVSAR['UD'][thisIdx]) and ('U' == self.NAVSAR['UD'][thisIdx+1]):
                self.NAVSAR['AF'][thisIdx] = 0.02
            
            if ('U' == self.NAVSAR['UD'][thisIdx]):
                self.NAVSAR['AF'][thisIdx] = self.NAVSAR['AF'][thisIdx+1] + 0.02
                if (self.NAVSAR['AF'][thisIdx] > 0.2):
                    self.NAVSAR['AF'][thisIdx] = 0.2
                return np.max(self.NAVSAR['SAR-10'][thisIdx+1:]) - self.NAVSAR['AF'][thisIdx]*(np.max(self.NAVSAR['SAR-10'][thisIdx+1:])-np.min(self.NAVSAR['NAV'][thisIdx:]))
            elif ('D' == self.NAVSAR['UD'][thisIdx]):
                self.NAVSAR['AF'][thisIdx] = self.NAVSAR['AF'][thisIdx+1] + 0.02
                if (self.NAVSAR['AF'][thisIdx] > 0.2):
                    self.NAVSAR['AF'][thisIdx] = 0.2
                return np.min(self.NAVSAR['SAR-10'][thisIdx+1:]) + self.NAVSAR['AF'][thisIdx]*(np.max(self.NAVSAR['NAV'][thisIdx:]) - np.min(self.NAVSAR['SAR-10'][thisIdx+1:]))
        else:
            return 0
      
    """
    PSY Index
    """
    def getHistoryPSY(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('PSY-12', np.float), ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVPSY = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVPSY['Date'] = self.DateData
        self.NAVPSY['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVPSY['PSY-12'][kdx] = self.__CalculatePSY__(kdx, 12)
            
        for idx in range(2, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVPSY['Long/Short'][kdx] = "N"
            if (self.NAVPSY['PSY-12'][kdx] >= 80):
                self.NAVPSY['Long/Short'][kdx] = "L"
                if (self.NAVPSY['PSY-12'][kdx] >= 90):
                    self.NAVPSY['Long/Short'][kdx] = "super-L"
            elif (self.NAVPSY['PSY-12'][kdx] <= 20):
                self.NAVPSY['Long/Short'][kdx] = "S"
                if (self.NAVPSY['PSY-12'][kdx] <= 10):
                    self.NAVPSY['Long/Short'][kdx] = "super-S"
                
        
        H5FM.Data2H5(self.NAVPSY,  self.FileName, 'NAV_PsychologicalLine', groupName = self.GroupName, dtype = dataType)
    
    def __CalculatePSY__(self, thisIdx, day):
        count = 0
        if (thisIdx <= len(self.NAVData)-1-day):
             for idx in range(0, day):
                 if (self.NAVUpDown[thisIdx+idx] > 0):
                     count += 1
             return count*100/day
        else:
            return 0
    
    """
    OBV Index
    """
    def getHistoryOBV(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('OBV', np.float), ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVOBV = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVOBV['Date'] = self.DateData
        self.NAVOBV['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVOBV['OBV'][kdx] = self.__CalculateOBV__(kdx)
        
        for idx in range(2, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVOBV['Long/Short'][kdx] = "N"
            UpDown = self.NAVUpDown[kdx]
            
            if ((self.NAVOBV['OBV'][kdx]- self.NAVOBV['OBV'][kdx+1]> 0) and (UpDown < 1)):
                self.NAVOBV['Long/Short'][kdx] = "L"
            elif ((self.NAVOBV['OBV'][kdx]- self.NAVOBV['OBV'][kdx+1] > 0) and (UpDown > 1)):
                self.NAVOBV['Long/Short'][kdx] = "super-L"
            elif ((self.NAVOBV['OBV'][kdx]- self.NAVOBV['OBV'][kdx+1] < 0) and (UpDown > 1)):
                self.NAVOBV['Long/Short'][kdx] = "S"
            elif ((self.NAVOBV['OBV'][kdx]- self.NAVOBV['OBV'][kdx+1] < 0) and (UpDown < 1)):
                self.NAVOBV['Long/Short'][kdx] = "super-S"
            if ((self.NAVOBV['OBV'][kdx+1]- self.NAVOBV['OBV'][kdx+2] < 0) and (self.NAVOBV['OBV'][kdx]- self.NAVOBV['OBV'][kdx+1] > 0)):
                self.NAVOBV['Long/Short'][kdx] = "S->L"
            elif ((self.NAVOBV['OBV'][kdx+1]- self.NAVOBV['OBV'][kdx+2] > 0) and (self.NAVOBV['OBV'][kdx]- self.NAVOBV['OBV'][kdx+1] < 0)):
                self.NAVOBV['Long/Short'][kdx] = "L->S"
                
        H5FM.Data2H5(self.NAVOBV,  self.FileName, 'NAV_OnBalanceVolume', groupName = self.GroupName, dtype = dataType)
        
    def __CalculateOBV__(self,thisIdx):
        if (thisIdx < len(self.NAVData)-2):
            if (self.NAVData[thisIdx] > self.NAVData[thisIdx+1]):
                return self.NAVOBV['OBV'][thisIdx+1] + self.NAVData[thisIdx]
                
            if (self.NAVData[thisIdx] < self.NAVData[thisIdx+1]):
                return self.NAVOBV['OBV'][thisIdx+1] - self.NAVData[thisIdx]
            
            if (self.NAVData[thisIdx] == self.NAVData[thisIdx+1]):
                return self.NAVOBV['OBV'][thisIdx+1]
        else:
            return 0
    
    """
    Bias Index
    """
    def getHistoryBias(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('Bias-5', np.float),('Bias-10', np.float),('Bias-20', np.float),('Bias-70', np.float),
                             ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVBias = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVBias['Date'] = self.DateData
        self.NAVBias['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVBias['Bias-5'][kdx] = self.__CalculateBias__(kdx,5)
            self.NAVBias['Bias-10'][kdx] = self.__CalculateBias__(kdx,10)
            self.NAVBias['Bias-20'][kdx] = self.__CalculateBias__(kdx,20)
            self.NAVBias['Bias-70'][kdx] = self.__CalculateBias__(kdx,70)
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVBias['Long/Short'][kdx] = "N"
            if (self.NAVBias['Bias-5'][kdx] < -3):
                self.NAVBias['Long/Short'][kdx] = "S"
            if (self.NAVBias['Bias-10'][kdx] < -4.5):
                self.NAVBias['Long/Short'][kdx] = "S+"
            if (self.NAVBias['Bias-20'][kdx] < -7):
                self.NAVBias['Long/Short'][kdx] = "super-S"
            if (self.NAVBias['Bias-70'][kdx] < -11):
                self.NAVBias['Long/Short'][kdx] = "super-S+"
            if (self.NAVBias['Bias-5'][kdx] > 3.5):     
                self.NAVBias['Long/Short'][kdx] = "L"
            if (self.NAVBias['Bias-10'][kdx] > 5): 
                self.NAVBias['Long/Short'][kdx] = "L+"
            if (self.NAVBias['Bias-20'][kdx] > 8): 
                self.NAVBias['Long/Short'][kdx] = "super-L"
            if (self.NAVBias['Bias-70'][kdx] > 11): 
                self.NAVBias['Long/Short'][kdx] = "super-L+"
                
        H5FM.Data2H5(self.NAVBias,  self.FileName, 'NAV_Bias', groupName = self.GroupName, dtype = dataType)

    
    def __CalculateBias__(self,thisIdx,day):
        if (thisIdx <= len(self.NAVData)-day-1):
            if (np.mean(self.NAVData[thisIdx:thisIdx+day]) > 0):
                return (self.NAVData[thisIdx] - np.mean(self.NAVData[thisIdx:thisIdx+day]))*100/np.mean(self.NAVData[thisIdx:thisIdx+day])
            else:
                return np.NAN
        else:
            return 0
    
    """
    W%R Index
    """
    def getHistoryWR(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('WR-5', np.float), ('WR-10', np.float), ('WR-15', np.float),
                             ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVWR = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVWR['Date'] = self.DateData
        self.NAVWR['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVWR['WR-5'][kdx] = self.__CalculateWR__(kdx, 5)
            self.NAVWR['WR-10'][kdx] = self.__CalculateWR__(kdx, 10)
            self.NAVWR['WR-15'][kdx] = self.__CalculateWR__(kdx, 15)
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVWR['Long/Short'][kdx] = 'N'
            WR_5, WR_10, WR_15 = self.NAVWR['WR-5'][kdx], self.NAVWR['WR-10'][kdx], self.NAVWR['WR-15'][kdx]
            if (( WR_5 > 50) and (WR_10 > 50) and (WR_15 > 50)):
                self.NAVWR['Long/Short'][kdx] = 'L'
                if (( WR_5 > 80) and (WR_10 > 80) and (WR_15 > 80)):
                    self.NAVWR['Long/Short'][kdx] = 'super-L'
            if (( WR_5 < 50) and (WR_10 < 50) and (WR_15 < 50)):
                self.NAVWR['Long/Short'][kdx] = 'S'
                if (( WR_5 < 20) and (WR_10 < 20) and (WR_15 < 20)):
                    self.NAVWR['Long/Short'][kdx] = 'super-S'
                
        H5FM.Data2H5(self.NAVWR,  self.FileName, 'NAV_WilliamsOverbought', groupName = self.GroupName, dtype = dataType)
    
    def __CalculateWR__(self, thisIdx, day):
        if (thisIdx <= len(self.NAVData)-day-1):
            Diff = (np.max(self.NAVData[thisIdx:thisIdx+day]) - np.min(self.NAVData[thisIdx:thisIdx+day]))
            if (abs(Diff) > 0):
                return 100 -(np.max(self.NAVData[thisIdx:thisIdx+day]) - self.NAVData[thisIdx])*100/Diff
            else:
                return np.NAN
        else:
            return 0
    
    """
    MTM Index
    """
    def getHistoryMTM(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('MTM', np.float), ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVMTM = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVMTM['Date'] = self.DateData
        self.NAVMTM['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVMTM['MTM'][kdx] = self.__CalculateMTM__(kdx, 10)
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVMTM['Long/Short'][kdx] = 'N'
            if (self.NAVMTM['MTM'][kdx] > 0):
                self.NAVMTM['Long/Short'][kdx] = 'L'
            if (self.NAVMTM['MTM'][kdx] < 0):
                self.NAVMTM['Long/Short'][kdx] = 'S'
            
            
        H5FM.Data2H5(self.NAVMTM,  self.FileName, 'NAV_MomentomIndex', groupName = self.GroupName, dtype = dataType)
        
    def __CalculateMTM__(self, thisIdx, day):
        if (thisIdx <= len(self.NAVData)-day-1):
            return self.NAVData[thisIdx] - self.NAVData[thisIdx+day]
        else:
            return self.NAVData[thisIdx]
        
    """
    RSI Index
    """
    def getHistoryRSI(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('Up-5', np.float), ('Down-5', np.float), ('RS-5', np.float), ('RSI-5', np.float), 
                             ('Up-10', np.float), ('Down-10', np.float), ('RS-10', np.float), ('RSI-10', np.float),
                             ('Bull/Bear', h5py.special_dtype(vlen=str)), ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVRSI = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVRSI['Date'] = self.DateData
        self.NAVRSI['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVRSI['Up-5'][kdx], self.NAVRSI['Down-5'][kdx] = self.__CalculateUpDown__(kdx,5)
            if (self.NAVRSI['Down-5'][kdx] != 0):
                self.NAVRSI['RS-5'][kdx] = self.NAVRSI['Up-5'][kdx]/self.NAVRSI['Down-5'][kdx]
            else:
                self.NAVRSI['RS-5'][kdx] = self.NAVRSI['Up-5'][kdx]*100
            self.NAVRSI['RSI-5'][kdx] = 100-100/(1+self.NAVRSI['RS-5'][kdx])
            self.NAVRSI['Up-10'][kdx], self.NAVRSI['Down-10'][kdx] = self.__CalculateUpDown__(kdx,10)
            if (self.NAVRSI['Down-10'][kdx] != 0):
                self.NAVRSI['RS-10'][kdx] = self.NAVRSI['Up-10'][kdx] / self.NAVRSI['Down-10'][kdx]
            else:
                self.NAVRSI['RS-10'][kdx] = self.NAVRSI['Up-10'][kdx]*100
            self.NAVRSI['RSI-10'][kdx] = 100-100/(1+self.NAVRSI['RS-10'][kdx])
            
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            DiffRSI = self.NAVRSI['Up-5'][kdx] - self.NAVRSI['RSI-10'][kdx]
            self.NAVRSI['Bull/Bear'][kdx] = 'N'
            self.NAVRSI['Long/Short'][kdx] = 'N'
            if (DiffRSI > 0):
                self.NAVRSI['Bull/Bear'][kdx] = 'Bull'
            if (DiffRSI < 0):
                self.NAVRSI['Bull/Bear'][kdx] = 'Bear'
                
            if (self.NAVRSI['RSI-5'][kdx] > 80):
                self.NAVRSI['Long/Short'][kdx] = 'L'
                if (self.NAVRSI['RSI-5'][kdx] > 90):
                    self.NAVRSI['Long/Short'][kdx] = 'super-L'
                
            if (self.NAVRSI['RSI-5'][kdx] < 20):
                self.NAVRSI['Long/Short'][kdx] = 'S'
                if (self.NAVRSI['RSI-5'][kdx] < 10):
                    self.NAVRSI['Long/Short'][kdx] = 'super-S'
                    
        H5FM.Data2H5(self.NAVRSI,  self.FileName, 'NAV_RelativeStrengthIndex', groupName = self.GroupName, dtype = dataType)
        
        
    def __CalculateUpDown__(self, thisIdx, day):
        Up, Down = 0, 0
        if (thisIdx <= len(self.NAVData)-day):
            for idx in range(thisIdx, thisIdx + day):
                if (self.NAVUpDown[thisIdx] > 0):
                    Up += self.NAVUpDown[thisIdx]
                if (self.NAVUpDown[thisIdx] < 0):
                    Down += self.NAVUpDown[thisIdx]
            Up /= day
            Down /= day
        else:
            Up, Down = 0, 0
        
        return Up, Down
        
    
    """
    KD Index:
        
    """
    
    def getHistoryKD(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('RSV-9', np.float), ('K', np.float), ('D', np.float), 
                             ('Bull/Bear', h5py.special_dtype(vlen=str)), ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVKD = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVKD['Date'] = self.DateData
        self.NAVKD['NAV'] = self.NAVData
        
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVKD['RSV-9'][kdx] = self.__CalculateRSV__(kdx,9)
            
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            if (kdx == len(self.NAVData) -1):
                self.NAVKD['K'][kdx] = 0
                self.NAVKD['D'][kdx] = 0
            else:
                self.NAVKD['K'][kdx] = self.NAVKD['K'][kdx+1]*2/3 + self.NAVKD['RSV-9'][kdx]/3
                self.NAVKD['D'][kdx] = self.NAVKD['D'][kdx+1]*2/3 + self.NAVKD['K'][kdx]/3
                
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            DiffKD = self.NAVKD['K'][kdx] - self.NAVKD['D'][kdx]
            self.NAVKD['Bull/Bear'][kdx] = 'N'
            self.NAVKD['Long/Short'][kdx] = 'N'
            if ((DiffKD > 0) and (self.NAVKD['K'][kdx] > 80) and (self.NAVKD['D'][kdx] > 70)):
                self.NAVKD['Bull/Bear'][kdx] = 'Bull'
            if ((DiffKD < 0) and (self.NAVKD['K'][kdx] < 20) and (self.NAVKD['D'][kdx] < 30)):
                self.NAVKD['Bull/Bear'][kdx] = 'Bear'
            if (self.NAVKD['K'][kdx] > 80 and self.NAVKD['D'][kdx] > 70):
                self.NAVKD['Long/Short'][kdx] = 'L'
                if (self.NAVKD['D'][kdx] > 85):
                    self.NAVKD['Long/Short'][kdx] = 'super-L'
            if (self.NAVKD['K'][kdx] < 20 and self.NAVKD['D'][kdx] < 30):
                self.NAVKD['Long/Short'][kdx] = 'S'
                if (self.NAVKD['D'][kdx] < 15):
                    self.NAVKD['Long/Short'][kdx] = 'super-S'
        H5FM.Data2H5(self.NAVKD,  self.FileName, 'NAV_StochasticOscillator', groupName = self.GroupName, dtype = dataType)
            
    def __CalculateRSV__(self, thisIdx, day):
        if (thisIdx <= len(self.NAVData)-day):
            Diff = np.max(self.NAVData[thisIdx:thisIdx+day])-np.min(self.NAVData[thisIdx:thisIdx+day])
            if (Diff > 0):
                RSV = (self.NAVData[thisIdx] - np.min(self.NAVData[thisIdx:thisIdx+day]))*100/(Diff)
            else:
                RSV = 0
        else:
            RSV = 0
            
        return RSV
    
    """
    OSC Index
    """
    
    def getHistoryOSC(self): #h5py.special_dtype(vlen=str)
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), 
                             ('DI', np.float), ('EMA12', np.float), ('EMA26', np.float), 
                             ('DIF', np.float), ('MACD', np.float), ('OSC', np.float), 
                             ('Bull/Bear', h5py.special_dtype(vlen=str)), ('Long/Short', h5py.special_dtype(vlen=str))])
        self.NAVOSC = np.zeros((len(self.NAVData),), dtype=dataType)
        self.NAVOSC['Date'] = self.DateData
        self.NAVOSC['NAV'] = self.NAVData
        
        # Calculate the Demand Index, DI
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVOSC['DI'][kdx] = self.__CalculateDI__(kdx)   
        
        # Calculate the Exponential Moving Average, EMA
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            EMA12, EMA26= self.__CalculateEMA__(kdx)
            self.NAVOSC['EMA12'][kdx] = EMA12
            self.NAVOSC['EMA26'][kdx] = EMA26
            self.NAVOSC['DIF'][kdx] = EMA12 - EMA26
        
        # Calculate the Moving Average Convergence/Divergence, MACD
        for idx in range(0, len(self.NAVData)):
            kdx = len(self.NAVData) -1 - idx
            self.NAVOSC['MACD'][kdx] = self.__CalculateMACD__(kdx)
        
        # Calculate the MACD Bar/Oscillator, OSC 
        for idx in range(0, len(self.NAVData)):
            DIF = self.NAVOSC['DIF'][idx]
            MACD = self.NAVOSC['MACD'][idx]
            OSC = DIF - MACD
            self.NAVOSC['OSC'][idx] = OSC
            if ((DIF > 0) and (MACD > 0)):
                self.NAVOSC['Bull/Bear'][idx] = 'Bull'
            elif ((DIF < 0) and (MACD < 0)):
                self.NAVOSC['Bull/Bear'][idx] = 'Bear'
            else:
                if (DIF > 0):
                    self.NAVOSC['Bull/Bear'][idx] = 'Turn-Bull'
                else:
                    self.NAVOSC['Bull/Bear'][idx] = 'Turn-Bear'
            if ((OSC > 0) and (DIF and MACD > 0)):
                self.NAVOSC['Long/Short'][idx] = 'L'
            elif ((OSC < 0) and (DIF and MACD < 0)):
                self.NAVOSC['Long/Short'][idx] = 'S'
            else:
                self.NAVOSC['Long/Short'][idx] = 'N'
        H5FM.Data2H5(self.NAVOSC,  self.FileName, 'NAV_Oscillator', groupName = self.GroupName, dtype = dataType)
    
    def __CalculateMACD__(self, thisIdx):
        if (len(self.NAVData)-9 == thisIdx):
            MACD = np.mean(self.NAVOSC['DIF'][thisIdx:thisIdx+9])
        elif (thisIdx < len(self.NAVData)-9):
            MACD = self.NAVOSC['MACD'][thisIdx+1]*4/5 + self.NAVOSC['DIF'][thisIdx]*1/5
        else:
            MACD = 0
            
        return MACD
 
    def __CalculateDI__(self, thisIdx):
        #print(idx, len(self.NAVData))
        return (np.min(self.NAVData[thisIdx:])+np.max(self.NAVData[thisIdx:])+2*self.NAVData[thisIdx])*0.25
    
    
    def __CalculateEMA__(self, thisIdx):
        if (len(self.NAVData)-12 == thisIdx):
            EMA12 = np.mean(self.NAVOSC['DI'][thisIdx:thisIdx+12])
        elif (thisIdx < len(self.NAVData)-12):
            EMA12 = self.NAVOSC['EMA12'][thisIdx+1]*11/13 + self.NAVOSC['DI'][thisIdx]*2/13
        else:
            EMA12 = 0
        
        if (len(self.NAVData)-26 == thisIdx):
            EMA26 = np.mean(self.NAVOSC['DI'][thisIdx:thisIdx+26])
        elif (thisIdx < len(self.NAVData)-26):
            EMA26 = self.NAVOSC['EMA26'][thisIdx+1]*25/27 + self.NAVOSC['DI'][thisIdx]*2/27
        else:
            EMA26 = 0 
            
        return EMA12, EMA26
    
        """
        DI12 = 0
        DI26 = 0
        LDI12 = 0
        LDI26 = 0
        for idx in range(thisIdx, thisIdx+27):
            if ((idx >= thisIdx) and (idx <= thisIdx+11)):
                DI12 += self.__CalculateDI__(idx)
            if ((idx >= thisIdx+1) and (idx <= thisIdx+12)):
                LDI12 += self.__CalculateDI__(idx)
            if ((idx >= thisIdx) and (idx <= thisIdx+25)):
                DI26 += self.__CalculateDI__(idx)
            if ((idx >= thisIdx+1) and (idx <= thisIdx+26)):
                LDI26 += self.__CalculateDI__(idx)
        DI12 /= 12
        LDI12 /= 12
        DI26 /= 26
        LDI26 /= 26
        return (LDI12*11/13 + DI12*2/13), (DI26*25/27 + LDI26*2/27)
        """
        
        """
    def __CalculateOSC__(self, thisIdx):
        #DIF_MACD_OSC = np.zeros((1,), dtype = dataType)
        sumDIF = 0
        LsumDIF = 0
        for idx in range(thisIdx, thisIdx+10):
            if ((idx >= thisIdx) and (idx <= thisIdx+8)):
                EMA12, EMA26 = self.__CalculateEMA__(idx)
                sumDIF += (EMA12 - EMA26)
            if ((idx >= thisIdx+1) and (idx <= thisIdx+9)):
                LEMA12, LEMA26 = self.__CalculateEMA__(idx)
                LsumDIF += (LEMA12 - LEMA26)
        sumDIF /= 9
        LsumDIF /= 9
        MACD = LsumDIF*4/5 + sumDIF * 1/5
        EMA12, EMA26 = self.__CalculateEMA__(thisIdx)
        DIF = EMA12 - EMA26
        OSC = DIF - MACD
        #DIF_MACD_OSC['DIF'][0] = (EMA12 - EMA26)
        #self.OSC  =  DIF_MACD_OSC['DIF'][0] - self.NAVMACD  
        #DIF_MACD_OSC['MACD'][0] = self.NAVMACD
        #DIF_MACD_OSC['OSC'][0] = self.OSC
        #H5FM.Data2H5(DIF_MACD_OSC,  self.FileName, 'DIF_MACD_OSC', groupName = self.GroupName, dtype = dataType)
        return DIF, MACD, OSC
    """

"""								 
                                            pandas.DataFrame(self.NAVBias).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVMTM).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVMTM).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVMA).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVMA).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVOBV).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVOBV).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVOSC).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVOSC).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVPSY).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVPSY).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVQuartile).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVQuartile).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVRSI).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVRSI).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVSAR).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVSAR).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVWR).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVWR).loc[:,'NAV'],
                                            pandas.DataFrame(self.NAVKD).loc[:,'Date'],
                                            pandas.DataFrame(self.NAVKD).loc[:,'NAV']])
"""	