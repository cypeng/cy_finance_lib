# -*- coding: utf-8 -*-
"""
Created on 20190215

Ref.
https://pythondata.com/stock-market-forecasting-with-prophet/, stock prediction
https://research.fb.com/prophet-forecasting-at-scale/, prophet document

@author: user009
DataVer  - Author - Note
20190217 - C.Y. Peng - First Release, Using FB API - fbprophet to predict the fund NAV
20190221 - C.Y. Peng - Setting: Output Folder and ML Prediction
"""
from sys import path
path.append('../CommonLib/')
import H5FileManager as H5FM
import CommonUtility as CU
import numpy as np
#import time
import h5py
import math
#Prophet
from fbprophet import Prophet

#from sklearn import metrics
import matplotlib.pyplot as plt
import pandas as pd
import os

def main(ml_days_prediction, file_name, dateset_name, group_name, output_folder):
    FundList = H5FM.H52StringArray(file_name, dateset_name, groupName=group_name)
    for idx in range(0, len(FundList)):
        fund_name = FundList[idx]
        CU.createOutputFolder(output_folder + fund_name + "\\")
        FP = getFundFBMLPrediction(file_name, fund_name, group_name, ml_days_prediction, output_folder)
        FP.readData()
        FP.get_prophet_training_data()
        FP.prophet_prediction()

class getFundFBMLPrediction():
    def __init__(self, file_name, fund_name, folder, ml_days_prediction = 30, output_folder = '.\\Analysis\\'):
        self.FileName = file_name
        self.FundName = fund_name
        self.DatesetName = 'HistoryNAV'
        self.GroupName = folder +'/' + fund_name
        self.NAVMatrix = None
        self.NAVTrainingData = None
        self.days_prediction = ml_days_prediction
        self.output_folder = output_folder

    def readData(self):
        NAVMatrix = H5FM.H52Data(self.FileName, 'NAV_Matrix', groupName = self.GroupName)
        (DateData, NAVData, UpDonwData, UDPercentage, UDday) = zip(*NAVMatrix)
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float), ('Up/Down', np.float),
                             ('Up/Down-Percentage', h5py.special_dtype(vlen=str)),('Up/Down Day', np.float)])
        NAVMatrix = np.zeros((len(NAVData),), dtype=dataType)
        NAVMatrix['Date'][:] = np.array(DateData)
        NAVMatrix['NAV'][:] = np.array(NAVData)
        NAVMatrix['Up/Down'][:] = np.array(UpDonwData)
        NAVMatrix['Up/Down-Percentage'][:] = np.array(UDPercentage)
        NAVMatrix['Up/Down Day'][:] = np.array(UDday)
        self.NAVMatrix = NAVMatrix[::-1]

    def get_prophet_training_data(self):
        dataType = np.dtype([('Date', h5py.special_dtype(vlen=str)), ('NAV', np.float)])
        self.NAVTrainingData = np.zeros((len(self.NAVMatrix),), dtype=dataType)
        self.NAVTrainingData['Date'] = pd.to_datetime(self.NAVMatrix['Date'])
        self.NAVTrainingData['NAV'] = self.NAVMatrix['NAV']
        self.NAVTrainingData = pd.DataFrame(self.NAVTrainingData)
        self.NAVTrainingData = self.NAVTrainingData.reset_index().rename(columns={'Date': 'ds', 'NAV': 'y'})
        #print(self.NAVTrainingData)

    def prophet_prediction(self):
        self.NAVTrainingData['y'] = np.log(self.NAVTrainingData['y'])

        # Model Definition
        model = Prophet()
        # Model Training
        model.fit(self.NAVTrainingData)
        # Prediction Data Estiablishment
        future = model.make_future_dataframe(periods = self.days_prediction)
        future.tail()
        # Prediction
        forecast = model.predict(future)
        #print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']])

        #print(type(forecast['ds'][0]), forecast['ds'])
        #print(type(self.NAVTrainingData['ds'][0]), self.NAVTrainingData['ds'])

        forecast_ds = forecast['ds']
        forecast_y = np.exp(forecast['yhat'])
        plt.plot(forecast_ds, forecast_y)
        plt.grid()
        plt.fill_between(forecast_ds, np.exp(forecast['yhat_upper']), np.exp(forecast['yhat_lower']),
                         alpha=0.5, color='darkgray')
        plt.grid()
        plt.plot(self.NAVTrainingData['ds'], np.exp(self.NAVTrainingData['y']))

        #plt.plot(forecast['ds'], [self.NAVTrainingData['y'], np.exp(forecast['yhat'])])
        plt.xlabel('Date, year')
        plt.ylabel('Fund Net Asset Value')
        plt.legend(['Forecast','Original'])
        plt.show(block=False)
        plt.savefig(self.output_folder + self.FundName + '\\prophet_NAV_ML')
        plt.close()

        AE = (forecast_y - np.exp(self.NAVTrainingData['y']))
        #print(AE.describe())
        MSE = (AE**2).mean()
        MAE = np.fabs(AE).mean()
        #print("MSE:", MSE)
        #print("MAE:", MAE)
        file = open(self.output_folder + self.FundName + '\\prophet_NAV_ML_log.txt', 'w')
        file.write(str(AE.describe())+'\n')
        file.write('Mean Squared Error:' + str(MSE)+'\n')
        file.write('Mean Absolute Error:' + str(MAE)+'\n')
        file.close()
        print('ML Prediction Completed!')

"""
model.plot(forecast)
x = forecast['ds']
y = forecast['yhat']
#forecast.plot(x='ds', y='yhat')
plt.grid()
plt.plot(x, y)
plt.show(block=False)
plt.savefig(self.FundName + '_prophet_ML_model')
plt.close()
"""
